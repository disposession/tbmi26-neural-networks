global GWXSIZE;
global GWYSIZE;


Q = zeros(GWXSIZE, GWYSIZE, 4);


runs = 300;
eps = 0.8;
while(runs > 0)
    gwinit(1);
    s = gwaction(0);
    i = 0;
    while (~s.isterminal)
        % out = sample([1 2 3 4], [0.25 0.25 0.25 0.25]);
        [out, opt_action] = choose_action(Q, s.pos(1), s.pos(2),...
            [1,2,3,4], 1, [0.25 0.25 0.25 0.25], [1-eps eps]);
        previous = s;
        s = gwaction(out);
        if(s.isterminal)
            reward = 10;
        else
            reward = s.feedback;
        end

        % Remove this line
        alpha = 0.25;
        gamma = 0.9;
        % Previous estimate
        prev_est = (1-alpha) * Q(previous.pos(1), previous.pos(2), out);
        % Better estimate 
        best_est = alpha*(reward+gamma*max(Q(s.pos(1), s.pos(2), :)));
        Q(previous.pos(1), previous.pos(2), out) = prev_est + best_est;
        i = i + 1;
    end
    runs = runs-1;
    disp(runs);
end

actions = [];
gwinit(1);
s = gwaction(0);
i = 0;
while (~s.isterminal)
    % out = sample([1 2 3 4], [0.25 0.25 0.25 0.25]);
    [out, opt_action] = choose_action(Q, s.pos(1), s.pos(2),...
        [1,2,3,4], 1, [0.25 0.25 0.25 0.25], [1 0]);
    previous = s;
    s = gwaction(out);
    if(s.isterminal)
        reward = 10;
    else
        reward = s.feedback;
    end

    %Keep track of path
    actions(end+1,:) = [previous.pos(1), previous.pos(2), out];
    % Remove this line
    alpha = 0.25;
    gamma = 0.9;
    % Previous estimate
    prev_est = (1-alpha) * Q(previous.pos(1), previous.pos(2), out);
    % Better estimate 
    best_est = alpha*(reward+gamma*max(Q(s.pos(1), s.pos(2), :)));
%         if reward>0
%             disp(prev_est);
%             disp(best_est);
%             disp(previous.pos);
%             disp(out);
%             disp('------');
%         end
    Q(previous.pos(1), previous.pos(2), out) = prev_est + best_est;
    i = i + 1;
end

gwdraw();
for i=1:size(actions, 1)
    ithelement = actions(i, :);
    gwplotarrow([ithelement(1); ithelement(2)], ithelement(3));
end
