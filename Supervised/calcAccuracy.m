function [ acc ] = calcAccuracy( cM )
%CALCACCURACY Takes a confusion matrix amd calculates the accuracy

total = sum(cM(:));
correct = sum(diag(cM));
acc = (correct/total) * 100; % Replace with your own code

end

