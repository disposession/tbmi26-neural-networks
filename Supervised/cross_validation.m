%% This script will help you test out your kNN code

%% Select which data to use:

% 1 = dot cloud 1
% 2 = dot cloud 2
% 3 = dot cloud 3
% 4 = OCR data

for dataSetNr=1:3
    [X, D, L] = loadDataSet( dataSetNr );
    % You can plot and study dataset 1 to 3 by running:
    % plotCase(X,D)
    
    %% Select a subset of the training features

    numBins = 3; % I'll be doing 3cross validation
    numSamplesPerLabelPerBin = 100; % Number of samples per label per bin, set to inf for max number (total number is numLabels*numSamplesPerBin)
    selectAtRandom = false; % true = select features at random, false = select the first features

    [ Xt, Dt, Lt ] = selectTrainingSamples(X, D, L, numSamplesPerLabelPerBin, numBins, selectAtRandom );

    % Note: Xt, Dt, Lt will be cell arrays, to extract a bin from them use i.e.
    % XBin1 = Xt{1};    

    %% Use kNN to classify data
    % Set the number of neighbors
    accuracy_for_k = zeros(10,1);
    for k=1:10
        accuracy_mat = zeros(numBins);
        for i=1:numBins
            for j=1:numBins
                if i ~= j
                    LkNN = kNN(Xt{i}, k, Xt{j}, Lt{j});
                    %% Calculate The Confusion Matrix and the Accuracy
                    % The confusionMatrix
                    cM = calcConfusionMatrix( LkNN, Lt{j});

                    % The accuracy
                    acc = calcAccuracy(cM);
                    accuracy_mat(i,j) = acc;
               end
            end
        end
        accuracy_for_k(k) = sum(accuracy_mat(:)) / (numBins*numBins - numBins);
    end
    [best_value, for_k] = max(accuracy_for_k);
    disp(['For data set number ' num2str(dataSetNr) ' the best k value is ' num2str(for_k) ' for accuracy ' num2str(best_value)]);
    % disp(accuracy_mat);
end




