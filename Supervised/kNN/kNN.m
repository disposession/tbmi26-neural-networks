function [ labelsOut ] = kNN(X, k, Xt, Lt)
    %KNN Your implementation of the kNN algorithm
    %   Inputs:
    %               X  - Features to be classified
    %               k  - Number of neighbors
    %               Xt - Training features
    %               LT - Correct labels of each feature vector [1 2 ...]'
    %
    %   Output:
    %               LabelsOut = Vector with the classified labels

    % disp(X);
    % disp(k);
    % disp(Xt);
    % disp(Lt);


    labelsOut  = zeros(size(X,2),1);
    classes = unique(Lt);
    numClasses = length(classes);

    % mdl = fitcknn(Xt', Lt, 'NumNeighbors', k, 'Distance', 'euclidean');
    % labelsOut = predict(mdl,X');

    % Take first pair from X
    for i=1:size(X, 2)
        pair = X(:, i);
        % Compare it with all training data pairs
        distances = zeros(size(Xt, 2), 1);
        for j=1:size(Xt, 2)
            training_pair = Xt(:, j);
            % store distance in array (euclidean distance)
            distances(j) = norm(training_pair - pair);
        end
        % sort that array whilst keeping the original position
        [sorted_distances, original_index] = sort(distances);
        klabels = zeros(k, 1);
        for j=1:k
            % find the original label of those points 
            % (through their original position)
            klabels(j) = Lt(original_index(j));
        end
        % the most used label wins is the label for this pair
        [most_used_label, k] = mode(klabels);
        
        labelsOut(i) = most_used_label;
    end
end