function [ Z, L ] = runSingleLayer(X, W)
%EVALUATESINGLELAYER Summary of this function goes here
%   Inputs:
%               X  - Features to be classified (matrix)
%               W  - Weights of the neurons (matrix)
%
%   Output:
%               Y = Output for each feature, (matrix)
%               L = The resulting label of each feature, (vector) 

Z = W * X;
% Run the activation function (not needed)
% A = tanh(Z);
% Calculate classified labels (Hint, use the max() function)
[~, L] = max(Z,[],1);
L = L(:);
end

