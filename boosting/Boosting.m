% Load face and non-face data and plot a few examples
load faces, load nonfaces
faces = double(faces); nonfaces = double(nonfaces);

% Generate Haar feature masks
nbrHaarFeatures = 75;
haarFeatureMasks = GenerateHaarFeatureMasks(nbrHaarFeatures);
figure(3)
colormap gray
for k = 1:25
subplot(5,5,k),imagesc(haarFeatureMasks(:,:,k),[-1 2])
axis image,axis off
end
% Create a training data set with a number of training data examples
% from each class. Non-faces = class label y=-1, faces = class label y=1
nbrTrainExamples = 2500;
trainImages = cat(3,faces(:,:,1:nbrTrainExamples/2),nonfaces(:,:,1:nbrTrainExamples/2));
xTrain = ExtractHaarFeatures(trainImages,haarFeatureMasks);
marked = zeros(size(xTrain, 1), size(xTrain, 2));
%disp(xTrain);
disp(size(xTrain));

% labels, expected output, etc.
yTrain = [ones(1,nbrTrainExamples/2), -ones(1,nbrTrainExamples/2)];

nbrWeakClassifiers = 200;%250;

% AdaBoost here!
weights = ones(nbrTrainExamples, nbrWeakClassifiers) * 1/nbrTrainExamples;

alphas = zeros(1, nbrWeakClassifiers);
bests = zeros(1, nbrWeakClassifiers);
incs = zeros(1, nbrWeakClassifiers);
threshes = zeros(1, nbrWeakClassifiers);
indAs = zeros(1, nbrWeakClassifiers);
indBs = zeros(1, nbrWeakClassifiers);
signs = zeros(1, nbrWeakClassifiers);
% sums = zeros(1, nbrWeakClassifiers);
weightSums = zeros(1, nbrWeakClassifiers);
feature = zeros(1, nbrWeakClassifiers);

step = 5;
scores = zeros(1, floor(nbrWeakClassifiers/step));

% for testing.
nbrTestExamplesF = size(faces, 3) - (nbrTrainExamples/2);
nbrTestExamplesNF = size(nonfaces, 3) - (nbrTrainExamples/2);
nbrTestExamples = nbrTestExamplesF + nbrTestExamplesNF;
startPos = nbrTrainExamples/2 + 1;
endPosF = size(faces, 3);
endPosNF = size(nonfaces, 3);
testImages = cat(3,faces(:,:,startPos:endPosF),nonfaces(:,:,startPos:endPosNF));
xTest = ExtractHaarFeatures(testImages,haarFeatureMasks);
disp(size(xTest));
yTest = [ones(1,nbrTestExamplesF), -ones(1,nbrTestExamplesNF)];
% ------------------------------------

% train the classifiers, incrementally check performance.
for i=1:nbrWeakClassifiers
    weightSums(i) = sum(weights(:, i));
    [best, index, sign, inc] = trainClassifier(weights(:, i), xTrain, yTrain, marked);
    bests(i) = best;
    marked(index(1), index(2)) = 1;
    indAs(i) = index(1);
    indBs(i) = index(2);
    incs(i) = inc;
    alphas(i) = alpha(best);
    signs(i) = sign;
    threshes(i) = xTrain(index(1), index(2));
    feature(i) = index(1);
%     if best == 0
%         nbrWeakClassifiers = i;
%         break;
%     end
    if i ~= nbrWeakClassifiers
        weights = updateWeights(weights, i, xTrain, yTrain, index(1), index(2));
    end
    
    if mod(i, step) == 0
        disp(strcat(int2str(i), '/', int2str(nbrWeakClassifiers)));
        accuracyTest = stronglyClassify(xTest, yTest, alphas(1:i), threshes(1:i), feature(1:i), signs(1:i));
        scores(floor(i/step) + 1) = accuracyTest;
        disp(accuracyTest);
    end
end

% Test the strong classifier!



readability = vertcat(alphas, bests, incs, threshes, indAs, indBs, signs, feature, weightSums);
% redStrings = ['alphas', 'bests', 'incs', 'threshes', 'indAs',  'signs', 'feature', 'weightSums'];
% readability = horzcat(redStrings, readability);

% disp('----weight sum----');
% disp(weightSums);
% disp('------------------');
% disp('-----------Alphas------------');
% disp(alphas);
% disp('-----------------------------');
% disp('----------Threshes-----------');
% disp(threshes);
% disp('-----------------------------');
% disp('------------Signs------------');
% disp(signs);
% disp('-----------------------------');
% disp('------------Sums-------------');
% disp(sums);
% disp('-----------------------------');
% disp('--------Output Labels--------');
% disp(outputLabels);
% disp('-----------------------------');

% finalPercentage = stronglyClassify(xTest, yTest, alphas, threshes, feature, signs);
% disp('-----------Output------------');
% % disp(finalOutput);
% disp(finalPercentage);
% disp('-----------------------------');

incorrectFaces = startPos + find(outputLabels(:,1:nbrTestExamplesF) ~= 1);
incorrectNonFaces = startPos + find(outputLabels(:,(nbrTestExamplesF + 1):nbrTestExamples) ~= -1);

figure(1)
colormap gray
for k=1:25
    subplot(5,5,k), imagesc(faces(:,:,incorrectFaces(k))), axis image, axis off
end
figure(2)
colormap gray
for k=1:25
    subplot(5,5,k), imagesc(nonfaces(:,:,incorrectNonFaces(k))), axis image, axis off
end

