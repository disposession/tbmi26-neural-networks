function [out] = alpha(x)

    out = log((1 - x)./x)./2;
end