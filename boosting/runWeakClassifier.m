function [labels] = runWeakClassifier(xTrain, f, s, sign)

    labels = ((xTrain(f, :) <= xTrain(f, s))*2 - 1) * sign;
end