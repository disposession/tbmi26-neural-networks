function [result] = stronglyClassify(xTest, yTest, alphas, threshes, feature, signs)

    outputLabels = ones(1, size(xTest, 2));
    for j=1:size(xTest, 2)
        sample = xTest(:,j);
        total = 0;
        for i=1:size(threshes, 2);
            total = total + alphas(i) * weaklyClassify(threshes(i), feature(i), sample, signs(i));
        end
        %sums(j) = total;
        outputLabels(j) = (total > 0) * 2 - 1;
    end
    finalOutput = yTest == outputLabels;
    result = (sum(finalOutput(:))/size(xTest, 2));
end