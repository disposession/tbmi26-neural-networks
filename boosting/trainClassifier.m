function [best, indexOfBest, signOfBest, incOfBest] = trainClassifier(weights, xTrain, yTrain, marked)

    % For every haar feature, divide all samples by that feature.
    best = 1.0;
    indexOfBest = [1, 1];
    signOfBest = -1;
    incOfBest = -1;
    [numFeats, numSamples] = size(xTrain);
    for f=1:numFeats
        for s=1:numSamples
            [err, ~, sign, inc] = weakClassifierError(weights, xTrain, yTrain, f, s );
            if err < best && marked(f, s) == 0
                best = err;
                indexOfBest = [f,s];
                signOfBest = sign;
                incOfBest = inc;
            end
        end
    end
    % Somehow these drop beneath 0. Wtf?
    if best < 0
        best = 0;
    end
end