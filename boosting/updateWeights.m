function [weights] = updateWeights(weights, t, xTrain, yTrain, f, s)
    
    [err, labels, ~, ~] = weakClassifierError(weights(:, t), xTrain, yTrain, f, s);
    % set weights
    temp = weights(:, t) .* exp(-alpha(err) * labels)';
    % threshold, prevent super-huge weights
    maxWeight = 20/size(weights, 1);
    % turns out we also need to keep a minimum.
    minWeight = 1/(size(weights, 1) * 1000000);
    temp(temp > maxWeight) = maxWeight;
    temp(temp < minWeight) = minWeight;
    % normalize
    temp = temp./sum(temp);
    weights(:, t + 1) = temp;
end