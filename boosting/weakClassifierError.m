function [err, labels, sign, inc] = weakClassifierError(weights, xTrain, yTrain, f, s)
    sign = 1;
    labels = runWeakClassifier(xTrain, f, s, sign);
    incorrect = yTrain ~= labels;
    inc = sum(incorrect(:));
    weightedIncorrect = incorrect.*weights';
    err = sum(weightedIncorrect);
    if( err > 0.5)
        sign = -1;
        err = 1 - err;
    end
end