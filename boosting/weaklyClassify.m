function[label] = weaklyClassify(thresh, feature, sample, sign)
    
    label = ((sample(feature) <= thresh)*2 - 1)*sign;
end