load countrydata
%% PDA
% Mean normalize the data to zero for each feature (row)
% Apply feature scaling just in case the different features differ highly
% in scale. Do this for each feature (row)
X=zeros(size(countrydata));
for i=1:size(countrydata,1)
    X(i,:) = (countrydata(i,:)-mean(countrydata(i,:)))/std(countrydata(i,:));
end

% Calculate the covariance matrix
cov = X*X';
cov = cov/(size(X, 2)-1);
% Calculate the correlation matrix
corr = zeros(size(cov));
for i=1:size(cov,1)
    for j=1:size(cov,2)
        corr(i, j) = cov(i,j) / sqrt(cov(i,i) * cov(j,j));
    end
end

%disp(cov);
figure(1);
imagesc(cov);
% imagesc(corr);
colorbar;

[A, B] = sorteig(cov);
disp(B);
Areduce = A(1:5, :);
Z = Areduce * X;
% disp(Z);
figure(1)
scatter3(Z(1, :), Z(2,:), Z(3,:), 50, countryclass);
figure(2)
scatter(Z(1, :), Z(2,:), 50, countryclass);


%% LDA
% classA = countryclass == 0;
% classB = countryclass == 1;
% classC = countryclass == 2;
% 
% classAdata = X(:, classA);
% classBdata = X(:, classB);
% classCdata = X(:, classC);
% 
% classAlabels = countries(classA, :);
% classBlabels = countries(classB, :);
% classClabels = countries(classC, :);
% 
% classAmeans = mean(classAdata, 2);
% classBmeans = mean(classBdata, 2);
% classCmeans = mean(classCdata, 2);
% 
% M = (classAmeans - classCmeans) * (classAmeans - classCmeans)';
% 
% normalizedClassAmeans=zeros(size(classAdata));
% for i=1:size(classAdata,1)
%     normalizedClassAmeans(i,:) = (classAdata(i,:)-classAmeans(i))/std(classAdata(i,:));
% end
% 
% normalizedClassCmeans=zeros(size(classCdata));
% for i=1:size(classCdata,1)
%     normalizedClassCmeans(i,:) = (classCdata(i,:)-classCmeans(i))/std(classCdata(i,:));
% end
% 
% Ca = (normalizedClassAmeans * normalizedClassAmeans')/(size(normalizedClassAmeans, 2)-1);
% Cc = (normalizedClassCmeans * normalizedClassCmeans')/(size(normalizedClassCmeans, 2)-1);
% Ctot = Ca + Cc;
% 
% W = Ctot \ (classAmeans - classCmeans);
% 
% % figure(2);
% % imagesc(Ctot);
% 
% % c = W'*X/norm(W)^2*W;
% new_X = horzcat(normalizedClassAmeans, normalizedClassCmeans);
% b = norm(W)^2; 
% Z1 = W' * new_X / b;
% c = W * Z1; 
% % plot(Z1);
% hold on;
% plotv(W([1 2]), '-');
% scatter(c(1, :), c(2,:), 50, horzcat(zeros(1,size(normalizedClassAmeans,2)), 2*ones(1,size(normalizedClassCmeans,2))));
% hold off;
